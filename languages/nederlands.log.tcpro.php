<?php
/**
 * nederlands.log.tcpro.php
 *
 * Dutch log language file
 *
 * @package TeamCalPro
 * @version 3.6.015
 * @author George Lewe <george@lewe.com>
 * @copyright Copyright (c) 2004-2014 by George Lewe
 * @link http://www.lewe.com
 * @license http://tcpro.lewe.com/doc/license.txt Based on GNU Public License v3
 */

/**
 * Log messages
 */
$LANG['log_abs_created'] = 'Absentietype aangemaakt: ';
$LANG['log_abs_updated'] = 'Absentietype bijgewerkt: ';
$LANG['log_abs_deleted'] = 'Absentietype verwijderd: ';
$LANG['log_user_added'] = 'Gebruiker toegevoegd: ';
$LANG['log_user_updated'] = 'Gebruikersprofiel bijgewerkt: ';
$LANG['log_user_deleted'] = 'Gebruiker verwijderd: ';
$LANG['log_user_archived_deleted'] = 'Gearchiveerde gebruiker verwijderd: ';
$LANG['log_user_archived'] = 'Gebruiker gearchiveerd: ';
$LANG['log_user_restored'] = 'Gebruiker teruggezet: ';
$LANG['log_user_registered'] = 'Gebruiker geregistreerd: ';
$LANG['log_user_pwd_reset'] = 'Wachtwoord hersteld: ';
$LANG['log_user_allow_updated'] = 'Verlofrechten bijgewerkt: ';
$LANG['log_user_avatar_updloaded'] = 'Gebruikersafbeelding ge&uuml;pload: ';
$LANG['log_user_group_updated'] = 'Gebruikerstype en/of groepstoewijzing bijgewerkt';
$LANG['log_user_verify_approval'] = 'Gebruiker geverifieerd, goedkeuring nodig: ';
$LANG['log_user_verify_unlocked'] = 'Gebruiker geverifieerd, ontgrendeld en zichtbaar gemaakt: ';
$LANG['log_user_verify_mismatch'] = 'Gebruikerverificatiecode komt niet overeen: ';
$LANG['log_user_verify_usr_notexist'] = 'Gebruiker bestaat niet: ';
$LANG['log_user_verify_code_notexist'] = 'Verificatiecode bestaat niet: ';
$LANG['log_ann_confirmed'] = 'Mededeling bevestigd: ';
$LANG['log_ann_all_confirmed_by'] = 'Alle mededelingen bevestigd door ';
$LANG['log_cal_fastedit'] = 'Kalender Snel Bewerken voor ';
$LANG['log_config'] = 'Configuratie gewijzigd';
$LANG['log_logout'] = 'Uitloggen';
$LANG['log_styles'] = 'Stijlen opnieuw opgebouwd';
$LANG['log_db_cleanup_before'] = 'Database opschonen voor ';
$LANG['log_db_delete_users'] = 'Database verwijderen: Alle gebruikers';
$LANG['log_db_delete_groups'] = 'Database verwijderen: Alle groepen';
$LANG['log_db_delete_regions'] = 'Database verwijderen: Alle regio\'s (behalve default)';
$LANG['log_db_delete_abs'] = 'Database verwijderen: Alle absentietypes';
$LANG['log_db_delete_hol'] = 'Database verwijderen: Alle vakanties';
$LANG['log_db_delete_daynotes'] = 'Database verwijderen: Alle algemene dagnotities';
$LANG['log_db_delete_ann'] = 'Database verwijderen: Alle mededelingen';
$LANG['log_db_delete_ann_orph'] = 'Database verwijderen: Alle niet gekoppelde mededelingen';
$LANG['log_db_delete_log'] = 'Database verwijderen: Logbestanden gewist';
$LANG['log_db_delete_archive'] = 'Database verwijderen: Archiefbestanden gewist';
$LANG['log_db_delete_perm'] = 'Database verwijderen: Alle aangepaste rechtenschema\'s';
$LANG['log_db_export'] = 'Database-export: ';
$LANG['log_db_restore'] = 'Database teruggezet van ';
$LANG['log_daynote_updated'] = 'Dagnotitie bijgewerkt: ';
$LANG['log_daynote_created'] = 'Dagnotitie aangemaakt: ';
$LANG['log_daynote_deleted'] = 'Dagnotitie verwijderd: ';
$LANG['log_decl_updated'] = 'Weigerinstellingen bijgewerkt: ';
$LANG['log_cal_usr_def_tpl'] = 'Standaard gebruikerssjabloon aangemaakt: ';
$LANG['log_cal_tplusr_def_tpl'] = 'Sjabloon-Standaard gebruikerssjabloon aangemaakt: ';
$LANG['log_cal_usr_tpl_chg'] = 'Gebruikerssjabloon gewijzigd: ';
$LANG['log_cal_usr_tpl_clr'] = 'Gebruikerssjabloon gewist: ';
$LANG['log_cal_declined'] = 'Kalenderwijziging geweigerd: ';
$LANG['log_month_tpl_created'] = 'Maandsjabloon aangemaakt: ';
$LANG['log_month_tpl_updated'] = 'Maandsjabloon bijgewerkt: ';
$LANG['log_group_created'] = 'Groep aangemaakt: ';
$LANG['log_group_updated'] = 'Groep bijgewerkt: ';
$LANG['log_group_deleted'] = 'Groep verwijderd: ';
$LANG['log_hol_created'] = 'Vakantie aangemaakt: ';
$LANG['log_hol_updated'] = 'Vakantie bijgewerkt: ';
$LANG['log_hol_deleted'] = 'Vakantie verwijderd: ';
$LANG['log_log_updated'] = 'Loginstellingen bijgewerkt';
$LANG['log_log_cleared'] = 'Log gewist';
$LANG['log_login_success'] = 'Inloggen succesvol';
$LANG['log_login_missing'] = 'Gebruikersnaam of wachtwoord ontbreekt';
$LANG['log_login_unknown'] = 'Gebruikersnaam onbekend';
$LANG['log_login_locked'] = 'Account vergrendeld';
$LANG['log_login_pwd'] = 'Wachtwoord onjuist';
$LANG['log_login_attempts'] = 'Te veel mislukte inlogpogingen';
$LANG['log_login_not_verified'] = 'Gebruikersaccount niet geverifieerd';
$LANG['log_login_ldap_pwd_missing'] = 'LDAP-wachtwoord ontbreekt';
$LANG['log_login_ldap_bind_failed'] = 'Koppelen met LDAP mislukt';
$LANG['log_login_ldap_connect_failed'] = 'Verbinden met LDAP mislukt';
$LANG['log_login_ldap_tls_failed'] = 'LDAP start TLS mislukt';
$LANG['log_login_ldap_username'] = 'LDAP-gebruikersnaam niet gevonden';
$LANG['log_login_ldap_search_bind_failed'] = 'LDAP zoek-koppeling mislukt';
$LANG['log_msg_email'] = 'E-mailbericht verzonden door ';
$LANG['log_msg_ann'] = 'Mededeling ';
$LANG['log_msg_ann_by'] = ' verzonden door ';
$LANG['log_perm_activated'] = 'Rechtenschema geactiveerd: ';
$LANG['log_perm_deleted'] = 'Rechtenschema verwijderd: ';
$LANG['log_perm_created'] = 'Rechtenschema aangemaakt: ';
$LANG['log_perm_reset'] = 'Rechtenschema hersteld: ';
$LANG['log_perm_changed'] = 'Rechtenschema gewijzigd: ';
$LANG['log_region_created'] = 'Regio aangemaakt: ';
$LANG['log_region_updated'] = 'Regio bijgewerkt: ';
$LANG['log_region_deleted'] = 'Regio verwijderd: ';
$LANG['log_region_merged'] = 'Regio\'s samengevoegd: ';
$LANG['log_region_ical'] = 'iCal-bestand "';
$LANG['log_csv_import'] = 'CSV gebruikersimport: ';
?>
